import os
from flask import (
    Blueprint, g, redirect, request, url_for, render_template,
    flash, current_app as app, json
)

from werkzeug.exceptions import abort
from tweet_it.auth import login_required
from tweet_it.models import User, Session
from werkzeug.utils import secure_filename


ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

bp = Blueprint('user', __name__, url_prefix="/user")


@bp.route('/profile/<int:id>', methods=('GET', 'POST'))
@login_required
def get_user(id):
    if request.method == 'GET':
        db = Session()
        current_user = db.query(User).filter_by(id=id).first()
        if current_user:
            return render_template('profile/profile.html', current_user=current_user)
        flash("User does not exits")
        return redirect(url_for('tweet.index'))
    return redirect(url_for('tweet.index'))

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def upload_file(req):
    error = None
    if 'file' not in req.files:
        error = 'No file part'
        flash(error)
        return redirect(req.url)
    
    filename = None
    file = req.files['file']
    if file.filename == '':
        error = 'No selected file'
        flash(error)
        return redirect(req.url)
    if error is None:
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.root_path, 'static/profiles' , filename))
    return filename

def field_validator(field):
    error = None
    if not field:
        error = f"{field} required"
        flash(error)
        return redirect(request.url)
    return error

@bp.route('/profile/<int:id>/update', methods=['GET', 'POST'])
@login_required
def update_user(id):
    if g.user.id != id:
        abort(403)
    if request.method == 'POST':
        error = None
        id = id
        name = request.form["name"]
        email = request.form["email"]
        bio = request.form["bio"]

        error = field_validator(name)
        error = field_validator(email)
        
        file_name = upload_file(request)  
        if file_name != None:
            db = Session()
            user = db.query(User).filter_by(id=g.user.id).first()
            user.id = id
            user.name = name
            user.email = email
            user.bio = bio
            user.profile_pic = 'static/profiles/{}'.format(file_name)
            db.commit()
            return redirect(url_for("user.get_user", id=id))
    return render_template("profile/update.html")
