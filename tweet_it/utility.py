from flask import (
    Blueprint, g, redirect, request, url_for, render_template,
    flash, current_app as app, json
)

from werkzeug.exceptions import abort
from tweet_it.auth import login_required
from tweet_it.models import User, Post, Session


bp = Blueprint('utility', __name__)

def like_or_dislike(id, likes):
    if g.user and request.method == "POST":
        db = Session()
        post = db.query(Post).filter_by(id=id).first()
        if post:
            if likes:
                post.likes = post.likes + 1
                db.commit()
                return json.dumps({"msg":"success", "status_code":200})
            else:
                post.dislikes = post.dislikes + 1
                db.commit()
                return json.dumps({"msg":"success", "status_code":200})
        return json.dumps({"error":"something went wrong", "status_code":404})
    return redirect(url_for("auth.login"))


@bp.route("/post/<int:id>/like", methods=["GET", "POST"])
@login_required
def get_like(id):
    return like_or_dislike(id, True)
        
@bp.route("/post/<int:id>/dislike", methods=["GET", "POST"])
@login_required
def get_dislike(id):
    return like_or_dislike(id, False)
