from flask import (
    Blueprint, g, redirect, request, url_for, render_template,
    flash, json, current_app as app
)

from tweet_it.search import query_index, add_to_index 
from tweet_it.auth import login_required
from tweet_it.models import  Post, Session


bp = Blueprint('elasticsearch', __name__)
sb = Session()

def elastic_search_index(db, model):
    tweets = db.query(model).all()
    for tweet in tweets:
        add_to_index("tweets", tweet)

def data_from_tweet(tweet):
    if tweet:
        return (tweet.content, tweet.created_at,tweet.users.profile_pic,
        tweet.likes, tweet.dislikes, tweet.users.name, tweet.id, tweet.users.id)
    return

@bp.route("/search", methods=["GET", "POST"])
@login_required
def search_tweets():
    if request.method == "POST":
        query = request.json
        error = None

        if query is None:
            return redirect(url_for("tweets.index"))
        db = Session()
        ids, total = query_index("tweets", query.strip(), 1, 100)
        
        queried_tweets = []
        if total == 0:
            return json.dumps({"status_code":404})
        else:
            for id in ids:
                tweet = db.query(Post).filter_by(id=id).first()
                tweet_result = data_from_tweet(tweet)
                queried_tweets.append(tweet_result)
            return json.dumps({"data":queried_tweets})
    return json.dumps({"error":"method not allowed"})