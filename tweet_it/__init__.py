import os

from flask import ( Flask, request, flash, redirect, url_for)
from tweet_it import ( auth, tweet, user, utility, elastic_search,
            file_download )
from elasticsearch import Elasticsearch

app = Flask(__name__)
app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY")
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

# Elastic search configuration
app.config["ELASTICSEARCH_URL"] = os.environ.get("ELASTICSEARCH_URL")
app.elasticsearch = Elasticsearch([app.config["ELASTICSEARCH_URL"]]) \
                if app.config['ELASTICSEARCH_URL'] else None


# Blueprints registartions
app.register_blueprint(auth.bp)
app.register_blueprint(tweet.bp)
app.add_url_rule("/", endpoint="index")
app.register_blueprint(user.bp)
app.register_blueprint(utility.bp)
app.register_blueprint(elastic_search.bp)
app.register_blueprint(file_download.bp)