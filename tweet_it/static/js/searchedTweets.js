document.addEventListener("DOMContentLoaded", () => {
    const searchForm = document.querySelector("#search")
    const mainBlock = document.querySelector(".main-block")
    
    searchForm.addEventListener("submit",  (e) => {
        e.preventDefault()
        let queryValue = e.currentTarget.children[0].value.trim()
        try {
            $.ajax({
                url: "http://localhost:5000/search",
                method: "POST",
                data:JSON.stringify(queryValue, null, '\t'),
                contentType: 'application/json;charset=UTF-8'
            }).done((data) => {
                if(data && data.statu_code != 404) {
                    tweets = JSON.parse(data)
                    console.log(tweets)
                    if(Array.isArray(tweets.data)) {
                        let filtered = tweets.data.filter(el => Boolean(el))
                        console.log(filtered)
                        if(filtered.length != 0) {
                            mainBlock.innerHTML = htmlBuilder(filtered)
                        } else {
                            mainBlock.innerHTML = `<h4>No tweets found</h4>`
                        }
                    } else {
                        mainBlock.innerHTML = `<h4>No tweets found</h4>`
                    }
                }
            })
            e.currentTarget.children[0].value = ""
                
        } catch (error) {
            console.log(error)
        }
    })    

})

function htmlBuilder(data) {
   return data.map(tweet => (`<div class="ui feed">
        <div class="event">
          <div class="label">
            <img class="ui avatar image" src="${ tweet[2] }">
          </div>
          <div class="content">
            <div class="summary">
              <a class="user"  href="user/profile/${ tweet[7] }">
                @${ tweet[5] }
              </a> 
              <div class="date time">
                ${ tweet[1] }
              </div>
              <div class="extra text">
                ${ tweet[0] }
              </div>
            </div>
            <div class="meta">
              <a class="main" id="like" data-id="${ tweet[6] }" >
                <i class="like icon main" style="color:red" ></i> <span> ${tweet[3]} Likes </span>
              </a>
              <a class="dis" data-id="${ tweet[6] }" >
                  <i class="thumbs down icon dis" style="color:blue"></i> <span> ${ tweet[4] } Dislikes </span>
              </a>
            </div>
          </div>
        </div>
    </div>
        `)
   ).join("")
}


