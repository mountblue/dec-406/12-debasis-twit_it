const times = document.querySelectorAll(".time")
Array.from(times).forEach(element => {
    element.innerText = moment(element.innerText).fromNow()
});