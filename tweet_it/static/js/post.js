document.addEventListener("DOMContentLoaded", () => {
    const likeButtons = document.querySelectorAll(".main")
    const dislikeButtons = document.querySelectorAll(".dis")
    
    applyAll(likeButtons, likeMe)
    applyAll(dislikeButtons, dislikeMe)

})

const applyAll = (btns, func) => {
    Array.from(btns).forEach( btn => {
        btn.addEventListener("click", func)
    });
} 

const likeMe = (e) =>  getLikeOrDislike(e , true)

const dislikeMe = (e) => getLikeOrDislike(e, false) 

const getLikeOrDislike = async (e, bool) => {
    try {
        let target = e.currentTarget
        const value = bool ? "like":"dislike";
        await $.ajax({
            url: `http://localhost:5000/post/${target.dataset.id}/${value}`,
            method: "POST"
        }).done((msg) => {
            console.log(JSON.parse(msg))
        })
        let innerIcon = target.children[0]
        let innerValue = target.children[1].innerText.trim().split(" ")
        
        if(bool) {
            Qty = parseInt(innerValue[0])
            target.children[1].innerText = `${Qty + 1} ${innerValue[1]} `

        } else {
            Qty = parseInt(innerValue[0])
            target.children[1].innerText = `${Qty + 1} ${innerValue[1]} `
        }


    } catch (error) {
        console.log(error)
    }
}
