import os
from flask import jsonify
from flask import (
    Blueprint, g, flash, json, current_app as app, redirect,
    url_for, request
)

from werkzeug.exceptions import abort
from tweet_it.models import User, Session
from tweet_it.auth import login_required
from tweet_it.flask_celery import download_file, celery_app

bp = Blueprint('celery', __name__)

@bp.route("/user/profile/download_tweets")
@login_required
def user_tweets():
    db  = Session()
    user_id = g.user.id
    user = db.query(User).filter_by(id=user_id).first()
    user_tweets = [tweet.jsonify_post()  for tweet in user.posts]
    async = download_file.delay([user_tweets, user.name, user.email])
    if async:
        return jsonify({"id":async.id})
    else:
        return jsonify({"status_code":404})

@bp.route("/user/profile/dowload_tweets", methods =['POST'])
@login_required
def get_file():
    id = request.form.get('id')
    if id is not None:
        response = download_file.AsyncResult(str(id))
        if response.ready():
            filename = response.get(timeout = 1)
            return jsonify({ "status":200, "filename":filename })
        return jsonify({"error":"something went wrong", "status":404 })
    return jsonify({"error":"id does not exits or not valid"})
    