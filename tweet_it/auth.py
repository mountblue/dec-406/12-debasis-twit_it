import functools

from tweet_it.models import Session, User
from werkzeug.security import (generate_password_hash, 
    check_password_hash
    )

from flask import (
    Blueprint, g, request, session, flash,
    url_for, redirect, render_template, abort
    )


bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        bio = request.form['bio']
        email = request.form['email']
        profile_pic = "https://semantic-ui.com/images/avatar/small/jenny.jpg"
        db = Session()
        error = None

        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'
        elif db.query(User).filter_by(name=username).first():
            error = f"{username} already exits"

        if error is None:
            hashed = generate_password_hash(password)
            user = User(name=username, password=hashed, email=email, bio=bio, profile_pic=profile_pic)
            db.add(user)
            db.commit()
            return redirect(url_for('auth.login'))
        flash(error)
    return render_template('auth/register.html')


@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = Session()
        user = db.query(User).filter_by(name=username).first()
        error = None
        
        if user is None:
            error = 'Incorrect username.'
        elif not check_password_hash(user.password, password):
            error = 'Incorrect password.'

        if error is None:
            session.clear()
            session['user_id'] = user.id
            return redirect(url_for('index'))
    
        flash(error)
    return render_template('auth/login.html')

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')
    if user_id is None:
        g.user = None
    else:
        db = Session()
        g.user = db.query(User).filter_by(id=user_id).first()


@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if session["user_id"] is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view