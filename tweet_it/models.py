import os
from datetime import datetime

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ( create_engine, Column, String, 
Boolean, Text, Integer, DateTime, LargeBinary,
ForeignKey, Sequence
)

from sqlalchemy.orm import relationship, sessionmaker

engine = create_engine(os.environ.get("DATABASE_URL"), pool_size=30, max_overflow=100) 
Session = sessionmaker(bind=engine)
Base = declarative_base()

def jsonify_model(instance):
    instance_json = {}
    for key, value in instance.__dict__.items():
        if key != "_sa_instance_state":
            instance_json[key] = value
    return instance_json

class User(Base):
    __tablename__ = "users"    
    id = Column(Integer, Sequence("user_id_seq") ,primary_key=True)
    name = Column(String(80), nullable=False)
    email = Column(String(100), nullable=False)
    bio = Column(String(200))
    profile_pic = Column(String(200))
    password = Column(String(200), nullable=False)
    admin = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.utcnow)
    posts = relationship("Post", backref="users",  lazy="subquery", cascade="all, delete-orphan")

    def __repr__(self):
        return f"{self.name}"

    def jsonify_user(self):
        return jsonify_model(self)
    
class Post(Base):
    __tablename__ = "posts"
    __searchable__ = ["content"]

    id = Column(Integer, Sequence("post_id_seq") ,primary_key=True)
    content = Column(String(140), nullable=False)
    likes = Column(Integer, default=0)
    dislikes = Column(Integer, default=0)
    author_id = Column(Integer, ForeignKey('users.id'))
    created_at = Column(DateTime, default=datetime.utcnow)
    comments = relationship("Comment", backref="posts",  lazy="subquery" , cascade="all, delete-orphan")

    def __repr__(self):
        return f"{self.id}"

    def jsonify_post(self):
        return jsonify_model(self)
        
class Comment(Base):
    __tablename__ = "comments"
    id = Column(Integer, Sequence("comment_id_seq") ,primary_key=True)
    content = Column(String(400))
    author_id = Column(Integer, ForeignKey('users.id'))
    post_id = Column(Integer, ForeignKey('posts.id'))
    created_at = Column(DateTime, default=datetime.utcnow)
    
    def __repr__(self):
        return f"{self.author_id} {self.post_id}"


def create_tables():
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)