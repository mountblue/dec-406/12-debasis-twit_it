import os
from celery import Celery
from flask import g, json

celery_app = Celery("tasks", backend="redis://localhost/" ,broker="redis://localhost/" )

@celery_app.task(name="celery_app.import")
def download_file(data):
    file_name = data[1] + ".json"
    path = os.path.join(os.getcwd(), 'static', 'user_tweets', file_name)   
    json_data = ""
    with open(path, "w") as tweet_file:
            json.dump(data, tweet_file )
    return file_name