from flask import (
    Blueprint, g, redirect, request, url_for, render_template,
    flash, session
)
from werkzeug.exceptions import abort
from sqlalchemy import desc
from tweet_it.auth import login_required
from tweet_it.models import Post, Session
from tweet_it.elastic_search import elastic_search_index
from tweet_it.search import remove_from_index 

bp = Blueprint('tweet', __name__)


@bp.route('/')
def index():
    db = Session()
    posts = db.query(Post).order_by(desc(Post.created_at)).all()
    most_liked = db.query(Post).order_by(desc(Post.likes)).limit(5).all()
    elastic_search_index(db, Post)
    return render_template("tweet/index.html", posts=posts, liked=most_liked)


@bp.route('/tweets/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        content = request.form['content']

        error = None

        if not content:
            error = 'Content is required.'
            
        if error is not None:
            flash(error)
        else:
            db = Session()
            post = Post(content=content.strip(), author_id=g.user.id)
            db.add(post)
            db.commit()
            elastic_search_index(db, Post)
            return redirect(url_for('tweet.index'))

    return render_template('tweet/create.html')


def get_post(id, db):
    post = db.query(Post).filter_by(id=id).first()
    
    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if post.author_id != session["user_id"]:
        abort(403, "Not authorized")

    return post

@bp.route('/tweets/<int:id>', methods=('GET', 'POST'))
@login_required
def get_post_by_id(id):
    db = Session()
    post = db.query(Post).filter_by(id=id).first()
    return render_template("tweet/tweet.html", post=post)

@bp.route('/tweets/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    db = Session()
    post = get_post(id, db)

    if request.method == 'POST':
        content = request.form['content']
        error = None

        if not content:
            error = 'Content is required.'

        if error is not None:
            flash(error)
        else:
            post.content=content.strip()
            db.commit()
            elastic_search_index(db, Post)
            return redirect(url_for('tweet.index'))

    return render_template('tweet/update.html', post=post)


@bp.route('/tweets/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    db = Session()
    post = get_post(id, db)
    db.delete(post)
    db.commit()
    remove_from_index("tweets",post)
    return redirect(url_for('tweet.index'))
